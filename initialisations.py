import numpy as np
import random as rng
import math
import functions as f

def InitializeFccSolidPos(u,N,D,L):
    """Fills first row of matrix u with initial FCC positions, for 4*N^D particles, in a box with length L 
    Matrix u is filled at [0,:,:], the initial position."""
    if (D!=3):
        print ("error D has to be equal to 3")
        return
    x=np.zeros([1,D,4*N**D],dtype=float)
    x01=np.arange(L/(4*N)-L/2,L/2,L/N,dtype=float)
    x0=np.concatenate((x01,(x01+L/(2*N))),axis=None)
    x1=np.arange(L/(4*N)-L/2,L/2,L/(2*N),dtype=float)
    x2=np.repeat(x1,2*N**2)
    x1=np.concatenate((x1,(np.flipud(x1))),axis=None)
    x1=np.repeat(x1,N)
    x1=np.tile(x1,N)
    x0=np.tile(x0,2*N**2)
    x[0,0,:]=x0
    x[0,1,:]=x1
    x[0,2,:]=x2
    u[0,:,:]=x
    return u

def InitializeFluidPos(x,D,L,n,MinDist):
    """Fills first row of matrix x with random positions, for n particles, in D dimensions, in a box with length L 
    Matrix x is filled at [0,:,:], the initial position."""
    i=0
    while i < n:
        x[0,:,i]=(np.random.rand(1,D)-0.5)*L
        x1=L*np.ones([1,n],dtype=float)
        for t in range (0,i):
            dl=f.SelectPart(x[0,:,t],x[0,:,i],D,L)
            x1[0,t]=f.r(dl)
        if np.amin(x1)>MinDist:
            i=i+1
            err=0
        elif err>200:
            print("Could not fit current atom. Try again or decrease the number of atoms. Currently %(n)s atoms were fit"%{"n":i})
            return
        else:
            err=err+1
    return(x)

def InitializeVelocity(v,D,L,n,T):
    """Fills first row of matrix v with random velocities from Maxwell distribution, for n particles, in D dimensions, in a box with length L 
    Matrix v is filled at [0,:,:], the initial velocity. This velocity is corrected to have zero net momentum."""
    for p in range (n):
        U=np.random.normal(0,math.sqrt(T),None) 
        up=np.random.rand(1,D)-0.5
        up=U*up/math.sqrt(np.sum(up**2))
        v[0,:,p]=up
    for i in range (D):
        v[0,i,:]=v[0,i,:]-np.mean(v[0,i,:])    #net initial momentum to zero
    return(v)

