import numpy as np
import random as rng
import math
def Ulj2(r):
    """Ulj2() calculates the dimensionless force from the dimensionless radius between two particles."""
    return 4*(-12*(r)**-14+6*(r)**-8)         # F=-dU/dr*1/r


def poscheck(x,i,D,n,L):
    """Poscheck() checks if one of n particles in D dimensions moves out of the box of size L, at timestep i and places it at the other side."""
    for d in range (0,D):
        for N in range(0,n):
            if abs(x[i+1,d,N])>L*.5:
                x[i+1,d,N]=x[i+1,d,N]-np.sign(x[i+1,d,N])*L        
    
def r(x):
    """Returns the norm of the vector x."""
    return np.linalg.norm(x)

def SelectPart(x,y,D,L):
    """SelectPart() checks which periodic position of a particle with vector y is nearest to the particle with vector x, 
    in D dimensions in a box of size L and should be used to calculate the force using the nearest neighbor algorithm ."""
    dx=np.ones([D],dtype=float)                   
    for N in range (0,D):
        y1=y[N]
        if abs(x[N]-(y[N]+np.sign(x[N])*L))<abs(y[N]-x[N]):
            y1=y[N]+np.sign(x[N])*L
        dx[N]=x[N]-y1
    return dx


def KinEn(v,i):
    """KinEn() calculates the total kinetic energy of the velocity matrix at step i."""
    return .5*np.sum(np.square(v[i,:,:]))

def PotEn(x,i,n,D,L):
    """PotEn() calculates the total potential energy from matrix x at step i, with n particles, D dimensions and a box with length L."""
    count=0
    for j in range(n):
        for k in range(n):
            if j!=k:
                r=np.linalg.norm(SelectPart(x[i,:,k],x[i,:,j],D,L))
                count=count+4*(r**-12-r**-6)
    return count/2

def Pot(r):
    """Returns the Lennard-Jones potential of a radius r."""
    return 4*(r**-12-r**-6)

def Bootstrap(data):
    """Takes a random resampling from data, with the same amount of values, from values in data, 
    some values might be taken twice or more, some are left out."""
    length=len(data)
    z=np.floor(np.random.uniform(0,length,length))
    p=np.zeros([length],dtype=float)
    for i in range(length):
        p[i]=data[int(z[i])]
    return p

def BootSTDTemp(data,nboot=200):
    """The values from data are resampled nboot times(200x standard) and the bootsrap standard deviation is calculated from it,
    this function works for the temperature and the energies."""
    A=np.zeros([nboot],dtype=float)
    for i in range(nboot):
        boots=Bootstrap(data)
        A[i]=np.mean(boots)
    A2=(np.mean(A))**2
    A2in=np.mean(np.square(A))
    return np.sqrt(A2in-A2)

def BootSTDCv(data,n,nboot=200):
    """The values from data are resampled nboot times(200x standard) and the bootsrap standard deviation is calculated from it,
    this function works for the specific heat."""
    A=np.zeros([nboot],dtype=float)
    for i in range(nboot):
        boots=Bootstrap(data)
        A[i]=CvCalc(boots,n)
    A2=(np.mean(A))**2
    A2in=np.mean(np.square(A))
    return np.sqrt(A2in-A2)

def CvCalc(data,n):
    """Calculates the specific heat from the kinetic energy values in data."""
    K2=(np.mean(data))**2
    K2in=np.mean(np.square(data))
    return 3*K2/(2*K2-3*n*(K2in-K2))